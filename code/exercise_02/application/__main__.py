from datetime import date

from kanban.domain.model.board import start_project
from kanban.domain.model.events import subscribe
from kanban.domain.model.workitem import register_new_work_item


def show_board(board):
    print()
    print("=" * 72)
    print("| board =", repr(board))
    for column_index, column in enumerate(board.columns()):
        print("|   column {} = {!r}".format(column_index, column))
        for workitem_id in column.work_item_ids():
            print("|     workitem_id =", workitem_id)
    print("=" * 72)
    print()


def main():
    subscribe(lambda event: True, print)

    # Setup a mission to Europa
    board = start_project(
        name="The Europa Mission",
        description="Search for life on Europa")

    board.add_new_column(name="To Do", wip_limit=None)
    board.add_new_column(name="Doing", wip_limit=3)
    board.add_new_column(name="Done", wip_limit=None)

    show_board(board)

    doing_column = board.column_with_name("Doing")
    board.insert_new_column_before(doing_column, name="Impeded", wip_limit=None)

    show_board(board)

    # Convert the Europa mission to a Mars mission
    board.name = "The Mars Mission"
    board.description = "Colonise Mars"

    impeded_column = board.column_with_name("Impeded")
    board.remove_column_by_name("Impeded")

    show_board(board)

    # TODO: These works items will be unreachable from the board, without a
    #       repository.  Perhaps we should keep a local mapping of work-item id
    #       to WorkItem that show_board() could use.

    work_item_1 = register_new_work_item(name="Design rocket",
                                         due_date=date(2020, 1, 9),
                                         content="The bigger the better.")

    work_item_2 = register_new_work_item(name="Build rocket",
                                         due_date=date(2020, 5, 12),
                                         content="Big goer upper.")

    work_item_3 = register_new_work_item(name="Launch rocket",
                                         due_date=None,
                                         content="Into space, towards Mars.")

    work_item_4 = register_new_work_item(name="Land on Mars",
                                         due_date=None,
                                         content="Gently does it!")

    work_item_5 = register_new_work_item(name="Construct habitat",
                                         due_date=date(2022, 3, 2),
                                         content="Home sweet home.")

    work_item_4.due_date = None

    board.schedule_work_item(work_item_1)
    board.schedule_work_item(work_item_2)
    board.schedule_work_item(work_item_3)
    board.schedule_work_item(work_item_4)
    board.schedule_work_item(work_item_5)

    show_board(board)

    board.abandon_work_item(work_item_1)

    board.advance_work_item(work_item_2)
    board.advance_work_item(work_item_3)

    show_board(board)

    board.advance_work_item(work_item_2)

    show_board(board)

    board.retire_work_item(work_item_2)

    show_board(board)

if __name__ == '__main__':
    main()
