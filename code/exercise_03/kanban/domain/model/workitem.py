import reprlib
import uuid

from kanban.domain.model.events import publish
from kanban.domain.model.entity import Entity


# ======================================================================================================================
# Aggregate root entity
#

class WorkItem(Entity):

    class Created(Entity.Created):
        pass

    def __init__(self, work_item_id, work_item_version, name, due_date, content):
        """DO NOT CALL DIRECTLY.
        """
        super().__init__(work_item_id, work_item_version)
        self.name = name
        self.due_date = due_date
        self.content = content

    def __repr__(self):
        return "{d}WorkItem(id={id!r}, name={name!r}, due_date={date!r}, content={content})".format(
            d="*Discarded* " if self._discarded else "",
            id=self.id,
            name=self._name,
            date=self._due_date and self._due_date.isoformat(),
            content=reprlib.repr(self._content))

    @property
    def name(self):
        self._check_not_discarded()
        return self._name

    @name.setter
    def name(self, value):
        self._check_not_discarded()
        if len(value) < 1:
            raise ValueError("WorkItem name cannot be empty")
        self._name = value
        event = Entity.AttributeChanged(originator_id=self.id,
                                        originator_version=self.version,
                                        name='_name',
                                        value=value)
        publish(event)
        self._increment_version()

    @property
    def due_date(self):
        """An optional due-date.

        If the work item has no due-date, this property will be None.

        Raises:
            DiscardedEntityError: When getting or setting, if this work item has been discarded.
        """
        self._check_not_discarded()
        return self._due_date

    @due_date.setter
    def due_date(self, value):
        self._check_not_discarded()
        self._due_date = value
        event = Entity.AttributeChanged(originator_id=self.id,
                                        originator_version=self.version,
                                        name='_due_date',
                                        value=value)
        publish(event)
        self._increment_version()

    @property
    def content(self):
        self._check_not_discarded()
        return self._content

    @content.setter
    def content(self, value):
        self._check_not_discarded()
        self._content = value
        event = Entity.AttributeChanged(originator_id=self.id,
                                        originator_version=self.version,
                                        name='_content',
                                        value=value)
        publish(event)
        self._increment_version()


# ======================================================================================================================
# Factories - the aggregate root factory
#

def register_new_work_item(name, due_date=None, content=None):
    work_item = WorkItem(work_item_id=uuid.uuid4().hex,
                         work_item_version=0,
                         name=name,
                         due_date=due_date,
                         content=content)

    event = WorkItem.Created(originator_id=work_item.id,
                             originator_version=work_item.version,
                             name=name,
                             due_date=work_item.due_date,
                             content=work_item.content)
    publish(event)
    return work_item
