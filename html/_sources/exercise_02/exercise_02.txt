Exercise 2 - Persistence
========================

In this exercise we store the event stream emitted by the domain model
to a file containing JSON representations of the events.

.. attention::

   The code for this exercise is in ``code/exercise_02/``

   The domain model code provided in this exercise is identical to what
   you *should* have ended up with at the end of exercise 1.  If you
   would like to proceed with your own code, you can copy your own
   versions of ``workitem.py`` and ``__main__.py`` from ``exercise_01``
   over the fresh versions in ``exercise_02``, although you should
   probably make a backup first.


Exercise 2a
-----------

We've added a new top-level package ``infrastructure`` to our system.
This contains a *very* simple event-store in
``infrastructure.event_store`` which simply stores 'events' in a file by
converting them to dictionaries and serialising them as JSON.  This
relies on some extensions to the Python Standard Library ``json``
encoder implemented in ``infrastructure.transcoders`` to handle ``date``
and ``datetime`` objects.

The ``infrastructure.persistence_subscriber`` module is responsible for
maintaining a binding between the domain events and the event store.  It
subscribes to all ``DomainEvent`` subclasses and appends them to the
event store in such a way that we can subsequently recover the class
name.

.. note::

   Although it would have been easier to use pickles rather than JSON
   we have chosen JSON because it is human readable.  Pickles are also
   too fragile with respect to module versions to be used for anything
   other than transient object serialisations and are innappropriate for
   a persistent store.

Familiarise yourself with the code of ``event_store.py`` and
``persistence_subscriber.py``.


Exercise 2b
-----------

Remove the subscription of ``print()`` to all events you added earlier in
Exercise 1f.

In its place, create an ``EventStore`` instance in the main function.
You can just use a filename like "store.events" as the ``store_path``
argument.

Then route all domain messages to the event store by creating a
``PersistenceSubscriber``. Remember to ``close()`` the
``PersistenceSubscriber`` at the end of the ``main()`` function.

..  admonition:: Solution
    :class: toggle

    .. code-block:: python
       :emphasize-lines: 1-2, 7-8, 12

       from infrastructure.event_store import EventStore
       from infrastructure.persistence_subscriber import PersistenceSubscriber

       # ...

       def main():
           event_store = EventStore("store.events")
           persistence_subscriber = PersistenceSubscriber(event_store)

           # ...

           persistence_subscriber.close()


Exercise 2c
-----------

Run the application to produce the ``event.store`` file.  Examine it at
the console or in your text editor.

..  admonition:: Solution
    :class: toggle

    .. code-block:: none

       $ cd exercise_02
       $ python3 -m application

         ...

       $ more store.events
       {"attributes":{"name":"_name","originator_id":"f10d1254cd43455a98c06ce2e4946431","originator_version":0,"timestamp":1440652323.452099,"value":"The Europa Mission"},"topic":"kanban.domain.model.entity#Entity.AttributeChanged"}
       {"attributes":{"name":"_description","originator_id":"f10d1254cd43455a98c06ce2e4946431","originator_version":1,"timestamp":1440652323.453316,"value":"Search for life on Europa"},"topic":"kanban.domain.model.entity#Entity.AttributeChanged"}
       {"attributes":{"description":"Search for life on Europa","name":"The Europa Mission","originator_id":"f10d1254cd43455a98c06ce2e4946431","originator_version":2,"timestamp":1440652323.453586},"topic":"kanban.domain.model.board#Board.Created"}
       {"attributes":{"name":"_name","originator_id":"5bedc773ed08482ca3c17b86a97c48c3","originator_version":0,"timestamp":1440652323.453898,"value":"To Do"},"topic":"kanban.domain.model.entity#Entity.AttributeChanged"}
       {"attributes":{"name":"_wip_limit","originator_id":"5bedc773ed08482ca3c17b86a97c48c3","originator_version":1,"timestamp":1440652323.454151,"value":null},"topic":"kanban.domain.model.entity#Entity.AttributeChanged"}
       {"attributes":{"column_id":"5bedc773ed08482ca3c17b86a97c48c3","column_name":"To Do","column_version":2,"originator_id":"f10d1254cd43455a98c06ce2e4946431","originator_version":2,"timestamp":1440652323.454404,"wip_limit":null},"topic":"kanban.domain.model.board#Board.NewColumnAdded"}
       {"attributes":{"name":"_name","originator_id":"3f9474752b264e1280c48cd4e9fa4802","originator_version":0,"timestamp":1440652323.454709,"value":"Doing"},"topic":"kanban.domain.model.entity#Entity.AttributeChanged"}
       {"attributes":{"name":"_wip_limit","originator_id":"3f9474752b264e1280c48cd4e9fa4802","originator_version":1,"timestamp":1440652323.454953,"value":3},"topic":"kanban.domain.model.entity#Entity.AttributeChanged"}
       {"attributes":{"column_id":"3f9474752b264e1280c48cd4e9fa4802","column_name":"Doing","column_version":2,"originator_id":"f10d1254cd43455a98c06ce2e4946431","originator_version":3,"timestamp":1440652323.4552,"wip_limit":3},"topic":"kanban.domain.model.board#Board.NewColumnAdded"}
       {"attributes":{"name":"_name","originator_id":"b315eaadd4c14a87ab2f515666c614c5","originator_version":0,"timestamp":1440652323.455501,"value":"Done"},"topic":"kanban.domain.model.entity#Entity.AttributeChanged"}
       {"attributes":{"name":"_wip_limit","originator_id":"b315eaadd4c14a87ab2f515666c614c5","originator_version":1,"timestamp":1440652323.455741,"value":null},"topic":"kanban.domain.model.entity#Entity.AttributeChanged"}
       {"attributes":{"column_id":"b315eaadd4c14a87ab2f515666c614c5","column_name":"Done","column_version":2,"originator_id":"f10d1254cd43455a98c06ce2e4946431","originator_version":4,"timestamp":1440652323.455985,"wip_limit":null},"topic":"kanban.domain.model.board#Board.NewColumnAdded"}
       {"attributes":{"name":"_name","originator_id":"961d1a2f2bdc4547887d428d86414a04","originator_version":0,"timestamp":1440652323.456562,"value":"Impeded"},"topic":"kanban.domain.model.entity#Entity.AttributeChanged"}
       {"attributes":{"name":"_wip_limit","originator_id":"961d1a2f2bdc4547887d428d86414a04","originator_version":1,"timestamp":1440652323.456834,"value":null},"topic":"kanban.domain.model.entity#Entity.AttributeChanged"}
       {"attributes":{"column_id":"961d1a2f2bdc4547887d428d86414a04","column_name":"Impeded","column_version":2,"originator_id":"f10d1254cd43455a98c06ce2e4946431","originator_version":5,"succeeding_column_id":"3f9474752b264e1280c48cd4e9fa4802","timestamp":1440652323.457085,"wip_limit":null},"topic":"kanban.domain.model.board#Board.NewColumnInserted"}
       {"attributes":{"name":"_name","originator_id":"f10d1254cd43455a98c06ce2e4946431","originator_version":6,"timestamp":1440652323.45751,"value":"The Mars Mission"},"topic":"kanban.domain.model.entity#Entity.AttributeChanged"}
       {"attributes":{"name":"_description","originator_id":"f10d1254cd43455a98c06ce2e4946431","originator_version":7,"timestamp":1440652323.457755,"value":"Colonise Mars"},"topic":"kanban.domain.model.entity#Entity.AttributeChanged"}
       {"attributes":{"column_id":"961d1a2f2bdc4547887d428d86414a04","originator_id":"f10d1254cd43455a98c06ce2e4946431","originator_version":8,"timestamp":1440652323.458008},"topic":"kanban.domain.model.board#Board.ColumnRemoved"}
       {"attributes":{"name":"_name","originator_id":"98c9039cffa444de89d00a13abe9401c","originator_version":0,"timestamp":1440652323.458888,"value":"Design rocket"},"topic":"kanban.domain.model.entity#Entity.AttributeChanged"}
       {"attributes":{"name":"_due_date","originator_id":"98c9039cffa444de89d00a13abe9401c","originator_version":1,"timestamp":1440652323.459398,"value":{"ISO8601_date":"2020-01-09"}},"topic":"kanban.domain.model.entity#Entity.AttributeChanged"}
       {"attributes":{"name":"_content","originator_id":"98c9039cffa444de89d00a13abe9401c","originator_version":2,"timestamp":1440652323.45999,"value":"The bigger the better."},"topic":"kanban.domain.model.entity#Entity.AttributeChanged"}
       {"attributes":{"content":"The bigger the better.","due_date":{"ISO8601_date":"2020-01-09"},"name":"Design rocket","originator_id":"98c9039cffa444de89d00a13abe9401c","originator_version":3,"timestamp":1440652323.460465},"topic":"kanban.domain.model.workitem#WorkItem.Created"}
       {"attributes":{"name":"_name","originator_id":"5a53884321234e4fa62b61bd538b200a","originator_version":0,"timestamp":1440652323.461098,"value":"Build rocket"},"topic":"kanban.domain.model.entity#Entity.AttributeChanged"}
       {"attributes":{"name":"_due_date","originator_id":"5a53884321234e4fa62b61bd538b200a","originator_version":1,"timestamp":1440652323.461568,"value":{"ISO8601_date":"2020-05-12"}},"topic":"kanban.domain.model.entity#Entity.AttributeChanged"}
       {"attributes":{"name":"_content","originator_id":"5a53884321234e4fa62b61bd538b200a","originator_version":2,"timestamp":1440652323.462027,"value":"Big goer upper."},"topic":"kanban.domain.model.entity#Entity.AttributeChanged"}
       {"attributes":{"content":"Big goer upper.","due_date":{"ISO8601_date":"2020-05-12"},"name":"Build rocket","originator_id":"5a53884321234e4fa62b61bd538b200a","originator_version":3,"timestamp":1440652323.462294},"topic":"kanban.domain.model.workitem#WorkItem.Created"}
       {"attributes":{"name":"_name","originator_id":"4890d907759444a4a098f5b0bc46ac00","originator_version":0,"timestamp":1440652323.462655,"value":"Launch rocket"},"topic":"kanban.domain.model.entity#Entity.AttributeChanged"}
       {"attributes":{"name":"_due_date","originator_id":"4890d907759444a4a098f5b0bc46ac00","originator_version":1,"timestamp":1440652323.462908,"value":null},"topic":"kanban.domain.model.entity#Entity.AttributeChanged"}
       {"attributes":{"name":"_content","originator_id":"4890d907759444a4a098f5b0bc46ac00","originator_version":2,"timestamp":1440652323.463194,"value":"Into space, towards Mars."},"topic":"kanban.domain.model.entity#Entity.AttributeChanged"}
       {"attributes":{"content":"Into space, towards Mars.","due_date":null,"name":"Launch rocket","originator_id":"4890d907759444a4a098f5b0bc46ac00","originator_version":3,"timestamp":1440652323.46344},"topic":"kanban.domain.model.workitem#WorkItem.Created"}
       {"attributes":{"name":"_name","originator_id":"c2d281084b114ad8bb9de296c04d4138","originator_version":0,"timestamp":1440652323.463722,"value":"Land on Mars"},"topic":"kanban.domain.model.entity#Entity.AttributeChanged"}
       {"attributes":{"name":"_due_date","originator_id":"c2d281084b114ad8bb9de296c04d4138","originator_version":1,"timestamp":1440652323.463962,"value":null},"topic":"kanban.domain.model.entity#Entity.AttributeChanged"}
       {"attributes":{"name":"_content","originator_id":"c2d281084b114ad8bb9de296c04d4138","originator_version":2,"timestamp":1440652323.464199,"value":"Gently does it!"},"topic":"kanban.domain.model.entity#Entity.AttributeChanged"}
       {"attributes":{"content":"Gently does it!","due_date":null,"name":"Land on Mars","originator_id":"c2d281084b114ad8bb9de296c04d4138","originator_version":3,"timestamp":1440652323.464444},"topic":"kanban.domain.model.workitem#WorkItem.Created"}
       {"attributes":{"name":"_name","originator_id":"6af724bcbf82460288949c40303292d9","originator_version":0,"timestamp":1440652323.465838,"value":"Construct habitat"},"topic":"kanban.domain.model.entity#Entity.AttributeChanged"}
       {"attributes":{"name":"_due_date","originator_id":"6af724bcbf82460288949c40303292d9","originator_version":1,"timestamp":1440652323.466125,"value":{"ISO8601_date":"2022-03-02"}},"topic":"kanban.domain.model.entity#Entity.AttributeChanged"}
       {"attributes":{"name":"_content","originator_id":"6af724bcbf82460288949c40303292d9","originator_version":2,"timestamp":1440652323.466429,"value":"Home sweet home."},"topic":"kanban.domain.model.entity#Entity.AttributeChanged"}
       {"attributes":{"content":"Home sweet home.","due_date":{"ISO8601_date":"2022-03-02"},"name":"Construct habitat","originator_id":"6af724bcbf82460288949c40303292d9","originator_version":3,"timestamp":1440652323.466678},"topic":"kanban.domain.model.workitem#WorkItem.Created"}
       {"attributes":{"name":"_due_date","originator_id":"c2d281084b114ad8bb9de296c04d4138","originator_version":3,"timestamp":1440652323.466986,"value":null},"topic":"kanban.domain.model.entity#Entity.AttributeChanged"}
       {"attributes":{"originator_id":"f10d1254cd43455a98c06ce2e4946431","originator_version":9,"timestamp":1440652323.467249,"work_item_id":"98c9039cffa444de89d00a13abe9401c"},"topic":"kanban.domain.model.board#Board.WorkItemScheduled"}
       {"attributes":{"originator_id":"f10d1254cd43455a98c06ce2e4946431","originator_version":10,"timestamp":1440652323.467497,"work_item_id":"5a53884321234e4fa62b61bd538b200a"},"topic":"kanban.domain.model.board#Board.WorkItemScheduled"}
       {"attributes":{"originator_id":"f10d1254cd43455a98c06ce2e4946431","originator_version":11,"timestamp":1440652323.467751,"work_item_id":"4890d907759444a4a098f5b0bc46ac00"},"topic":"kanban.domain.model.board#Board.WorkItemScheduled"}
       {"attributes":{"originator_id":"f10d1254cd43455a98c06ce2e4946431","originator_version":12,"timestamp":1440652323.467994,"work_item_id":"c2d281084b114ad8bb9de296c04d4138"},"topic":"kanban.domain.model.board#Board.WorkItemScheduled"}
       {"attributes":{"originator_id":"f10d1254cd43455a98c06ce2e4946431","originator_version":13,"timestamp":1440652323.468238,"work_item_id":"6af724bcbf82460288949c40303292d9"},"topic":"kanban.domain.model.board#Board.WorkItemScheduled"}
       {"attributes":{"column_index":0,"originator_id":"f10d1254cd43455a98c06ce2e4946431","originator_version":14,"priority":0,"timestamp":1440652323.468777,"work_item_id":"98c9039cffa444de89d00a13abe9401c"},"topic":"kanban.domain.model.board#Board.WorkItemAbandoned"}
       {"attributes":{"originator_id":"f10d1254cd43455a98c06ce2e4946431","originator_version":15,"priority":0,"source_column_index":0,"timestamp":1440652323.469239,"work_item_id":"5a53884321234e4fa62b61bd538b200a"},"topic":"kanban.domain.model.board#Board.WorkItemAdvanced"}
       {"attributes":{"originator_id":"f10d1254cd43455a98c06ce2e4946431","originator_version":16,"priority":0,"source_column_index":0,"timestamp":1440652323.469666,"work_item_id":"4890d907759444a4a098f5b0bc46ac00"},"topic":"kanban.domain.model.board#Board.WorkItemAdvanced"}
       {"attributes":{"originator_id":"f10d1254cd43455a98c06ce2e4946431","originator_version":17,"priority":0,"source_column_index":1,"timestamp":1440652323.47063,"work_item_id":"5a53884321234e4fa62b61bd538b200a"},"topic":"kanban.domain.model.board#Board.WorkItemAdvanced"}
       {"attributes":{"originator_id":"f10d1254cd43455a98c06ce2e4946431","originator_version":18,"timestamp":1440652323.471432,"work_item_id":"5a53884321234e4fa62b61bd538b200a"},"topic":"kanban.domain.model.board#Board.WorkItemRetired"}

Exercise 2d (bonus)
-------------------

Put the following lines of code into your program near the end of
``main()`` but *before* the ``PersistenceSubscriber`` is closed:

.. code-block:: python

   import code
   code.interact(local=dict(globals(), **locals()))

This will drop you to a REPL session from where you can manipulate the
domain model interactively.  While doing so, watch the events being
appended to the event store in another shell session with:

.. code-block:: shell

   $ tail -f event.store

.. hint::

   On Windows with Powershell 3 you can use::

     > Get-Contents event.store -Tail 10 -Wait

   Alternatively you can use the graphical
   `mTail <http://ophilipp.free.fr/op_tail.htm>`_ application

