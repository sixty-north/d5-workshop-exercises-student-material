# Deliver Domain Driven Designs. Dynamically! #

This repository contains the documentation and code for a workshop on building **Event-sourced Domain Models in Python**.

## License ##

Deliver Domain Driven Designs Dynamically (D5) by [Sixty North AS](http://sixty-north.com) is licensed under a [Creative Commons Attribution- NonCommercial-ShareAlike 4.0 International License](http://creativecommons.org/licenses/by-nc-sa/4.0/) based on a work at https://bitbucket.org/sixty-north/d5-workshop-exercises-student-material

## Getting Started ##

Using Git:

    $ git clone https://bitbucket.org/sixty-north/d5-workshop-exercises-student-material.git d5w

Or by downloading a zip file:

  * Navigate to the downloads sections (the cloud icon on the left)
  * Download using the *Download repository* link
  * Unzip on your system

Then view the HTML documentation:

    $ cd d5w
    $ open html/index/html